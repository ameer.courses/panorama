// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:panorama_p/UI/Screens/filter.dart';
import 'package:panorama_p/UI/Screens/login.dart';
import 'package:panorama_p/UI/Screens/profile.dart';

import 'UI/Screens/bestOffers.dart';
import 'UI/Screens/fav.dart';
import 'UI/Screens/home.dart';
import 'UI/Screens/search.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.light().copyWith(
        appBarTheme: AppBarTheme(
          elevation: 0,
          backgroundColor: Colors.indigo
        ),

      ),
      darkTheme: ThemeData.dark().copyWith(
          appBarTheme: AppBarTheme(
              elevation: 0,
              backgroundColor: Colors.blueGrey
          ),
      ),
      debugShowCheckedModeBanner: false,

      routes: {
        '/login' : (c) => LoginScreen(),
        '/home' : (c) => Home(),
        '/best' : (c) => BestOffers(),
        '/favorite' :(c) =>Favorite(),
        '/profile' : (c) => Profile(),
        '/search' : (c) => SearchScreen(),
        '/filter' : (c) => FilterScreen()
      },

      initialRoute: '/login',

    );
  }
}
