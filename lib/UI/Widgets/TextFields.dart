// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

class CustomField extends StatefulWidget {
  final String hint;
  final TextInputType keyboard;
  final IconData iconData;
  final bool isPass;
  bool _show = false;
  final TextEditingController controller;


  CustomField({
    required this.hint,
    required this.keyboard,
    required this.iconData,
    required this.controller,
    this.isPass = false
  });

  @override
  State<CustomField> createState() => _CustomFieldState();
}

class _CustomFieldState extends State<CustomField> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.all(size.width*0.035),
      padding: EdgeInsets.all(size.width*0.01),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(size.width*0.01),
      ),
      child: TextField(
        keyboardType: widget.keyboard,
        controller: widget.controller,
        style: TextStyle(
          color: Colors.black
        ),
        obscureText: !widget._show && widget.isPass,
        decoration: InputDecoration(
            border: InputBorder.none,
            hintText: widget.hint,
            hintStyle: TextStyle(
                color: Colors.grey
            ),
            prefixIcon: Icon(widget.iconData,color: Colors.blueGrey,),
            suffixIcon: widget.isPass ? IconButton(
                onPressed: (){
                  setState(() {
                    widget._show = !widget._show;
                  });
                },
                icon: widget._show ? Icon(Icons.visibility_off,color: Colors.blueGrey,) : Icon(Icons.visibility,color: Colors.blueGrey,)
            )
                :null
        ),

      ),
    );
  }
}
