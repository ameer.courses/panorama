// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HotelView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double h = size.height > size.width ? size.height :size.width;
    double w = !(size.height > size.width) ? size.height :size.width;
    return Container(
      width: double.infinity,
      height: h*0.25,
      child: Row(
        children: [
          Container(
            width: w*0.45,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: NetworkImage('https://media-cdn.tripadvisor.com/media/photo-s/16/1a/ea/54/hotel-presidente-4s.jpg'),
                fit: BoxFit.cover

              )
            ),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.all(w*0.01),
              child: Column(
                children: [
                RowData('Hotel Hotel Hotel Hotel Hotel ', Icons.account_balance_outlined, w),
                RowData('Latakia', Icons.location_on_outlined, w),
                RowData('5', Icons.star, w),
                RowData('1254', Icons.visibility_outlined, w),
                ],
              ),
            ),
          ),
          Container(
            width: w*0.15,
            alignment: Alignment.topCenter,
            child: IconButton(
              icon: Icon(Icons.favorite_border),
              onPressed: (){},
            ),
          ),
        ],
      ),
      margin: EdgeInsets.all(w*0.01),
      decoration: BoxDecoration(
        color: Theme.of(context).cardColor,
        boxShadow: [
          BoxShadow(
            color: Theme.of(context).cardColor,
            offset: Offset(-1,-1),
            blurRadius: 2
          ),
          BoxShadow(
            color: Theme.of(context).cardColor,
            offset: Offset(1,1),
            blurRadius: 2
          ),
        ]
      ),
    );
  }

  Widget RowData(String name,IconData icon,double w){
    if(name.length > 18)
      name = name.substring(0,15) + '...';
    return Expanded(
      child: Row(
        children: [
          Icon(icon),
          SizedBox(
            width: w*0.035,
          ),
          Expanded(child: Text(name))
        ],
      ),
    );
  }

}
