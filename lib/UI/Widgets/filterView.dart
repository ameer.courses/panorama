import 'package:flutter/material.dart';

class FilterView extends StatelessWidget {

  final List<String> filters;
  final String title;


  FilterView({required this.filters,required this.title});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Text(title),
          Wrap(
            children: [
              for(var filter in filters)
                FilterButton(
                  onTap: (){},
                  lable: filter,
                ),
            ],
          ),
        ],
      ),
    );
  }
}

class FilterButton extends StatelessWidget {

  final Function onTap;
  final String lable;


  FilterButton({required this.onTap,required this.lable});

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    final Color color = Theme.of(context).appBarTheme.backgroundColor!;
    return Container(
      padding: EdgeInsets.all(size.width*0.01),
      margin: EdgeInsets.all(size.width*0.01),
      child: Text(
        lable,

      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(size.width*0.01),
        border: Border.all(color: color)
      ),
    );
  }
}
