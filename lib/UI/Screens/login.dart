// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:panorama_p/UI/Widgets/TextFields.dart';
import 'Screen.dart';

class LoginScreen extends StatelessWidget {

  final TextEditingController emailController = TextEditingController();
  final TextEditingController passController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Screen(
        title: 'Login',
        body: Container(
          width: double.infinity,
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.all(size.width*0.025),
                child: CircleAvatar(
                  radius: size.width*0.25,
                  backgroundColor: Colors.white,
                  child: Lottie.asset('assets/animations/login.json'),
                ),
              ),
              CustomField(
                  hint: 'email',
                  keyboard: TextInputType.emailAddress,
                  iconData: Icons.email,
                  controller: emailController,
              ),
              CustomField(
                  hint: 'password',
                  keyboard: TextInputType.visiblePassword,
                  iconData: Icons.lock,
                  isPass: true,
                  controller: passController,
              ),

              ElevatedButton(
                  onPressed: (){
                    print("""
                    email : ${emailController.text}
                    pass  : ${passController.text}
                    """);
                    Navigator.pushReplacementNamed(context, '/home');
                  },
                  child: Text('Login')
              ),

            ],
          ),
        ),
    );
  }
}
