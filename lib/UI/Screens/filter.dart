import 'package:flutter/material.dart';
import 'package:panorama_p/UI/Screens/Screen.dart';
import 'package:panorama_p/UI/Widgets/filterView.dart';

class FilterScreen extends StatelessWidget {

  final List<Widget> content = [
    FilterView(
        filters: [
          for(int i = 0 ; i < 10 ; i++)
          '${i*100} - ${i*100 + i*200}',
        ], title: 'Budget'
    ),
    FilterView(
        filters: [
          for(int i = 0 ; i < 15 ; i++)
            '${i*100} - ${i*100 + i*200}',
        ], title: 'Budget'
    )

  ];

  @override
  Widget build(BuildContext context) {
    return Screen(
        title: 'Filter',
        body:ListView.separated(
            itemCount: content.length,
            itemBuilder: (context,i){
              return content[i];
            },
            separatorBuilder: (context,i){
              return Divider();
            },
        ),
    );
  }
}
