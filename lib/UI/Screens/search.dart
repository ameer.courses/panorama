// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:panorama/panorama.dart';

class SearchScreen extends StatefulWidget {
  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  List<String> history = [];

  final TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: TextField(
          controller: controller,
          decoration: InputDecoration(
              hintText: "Search",
              suffixIcon: IconButton(
                icon: Icon(Icons.search),
                onPressed: () {
                  setState(() {
                    if (controller.text.isNotEmpty) history.add(controller.text);
                  });
                },
              ),
              prefixIcon: IconButton(
                icon: Icon(Icons.close),
                onPressed: () {
                  controller.text = '';
                },
              )),
        ),
      ),
      body: ListView.separated(
        itemCount: history.length,
        itemBuilder: (context, i) {
          return ListTile(
            title: Text(history[i]),
            onTap: (){
              controller.text = history[i];
            },
          );
        },
        separatorBuilder: (context, i) {
          return Divider();
        },
      ),
    );
  }
}
