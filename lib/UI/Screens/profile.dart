// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:panorama_p/UI/Widgets/navBar.dart';

import 'Screen.dart';

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Screen(
      title: 'Profile',
      body: ListView.builder(
          itemCount: 20,
          itemBuilder: (context,i){
            return Card(
              child: ListTile(
                title: Text('hotel'),
                subtitle: Text('damascus'),
                leading: CircleAvatar(),
                trailing: Icon(Icons.favorite),
              ),
            );
          }
      ),
      bottomNavigationBar: NavBar(3),
    );
  }
}
