// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:panorama/panorama.dart';


class PanoramaScreen extends StatefulWidget {

  double longitude = 0;
  double latitude  = 0;
  double tilt      = 0;
  bool stop = false;
  @override
  State<PanoramaScreen> createState() => _PanoramaScreenState();
}

class _PanoramaScreenState extends State<PanoramaScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Panorama(
        child: Image.asset('assets/images/attack.jpg'),
        onViewChanged: (lon,lat,t){
          if(!widget.stop)
            setState(() {
              print("""
longitude : $lon
latitude  : $lat
tilt      : $t
          """);
              widget.latitude = lat;
              widget.longitude = lon;
              widget.tilt = t;
            });
        },
        hotspots: [
          Hotspot(
              latitude: widget.latitude,
              longitude: widget.longitude,

              widget: CircleAvatar(
                  child: IconButton(
                    icon: Icon(Icons.add),
                    onPressed: (){
                      widget.stop = true;
                    },
                  )
              )
          ),
        ],
      ),
    );
  }
}
