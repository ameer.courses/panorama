// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:panorama_p/UI/Widgets/navBar.dart';

class Screen extends StatelessWidget {
  final String title;
  final List<Widget>? actions;
  final Widget? leading;
  final Widget? body;
  final Drawer? drawer;
  final Widget? bottomNavigationBar;
  final int? index;
  Screen({required this.title,this.actions,this.leading,this.body,this.drawer,this.bottomNavigationBar,this.index});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        centerTitle: true,
        actions:(index != null)? [
          IconButton(
              onPressed: (){
                Navigator.pushNamed(context, '/search');
              },
              icon: Icon(Icons.search)
          )
        ]:actions,
        leading: leading,

      ),

      body:(index == null) ? body : Column(
        children: [
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                TextButton.icon(
                    onPressed: (){},
                    icon: Icon(CupertinoIcons.sort_down),
                    label: Text('Sort')
                ),
                TextButton.icon(
                    onPressed: (){
                      Navigator.pushNamed(context, '/filter');
                    },
                    icon: Icon(Icons.filter_list_alt),
                    label: Text('filter')
                ),
                TextButton.icon(
                    onPressed: (){},
                    icon: Icon(CupertinoIcons.home),
                    label: Text('popular')
                ),
              ],
            ),
          ),
          Expanded(
              flex: 9,
              child: body ?? Text('no Content')
          ),
        ],
      ),
      bottomNavigationBar:(index != null)? NavBar(index!) : bottomNavigationBar,
      drawer:(index != null)? Drawer(
        child: ListView(
          children: [
            DrawerHeader(
              child: Container(
                color: Theme.of(context).primaryColor,

              ),
              margin: EdgeInsets.zero,
              padding: EdgeInsets.zero,
            ),
            Card(
              child: ListTile(
                leading: Icon(Icons.search),
                title: Text('Search'),
                trailing: IconButton(
                  icon: Icon(Icons.arrow_forward_ios),
                  onPressed: (){},
                ),
              ),
            ),
            Card(
              child: ListTile(
                leading: Icon(Icons.search),
                title: Text('Search'),
                trailing: IconButton(
                  icon: Icon(Icons.arrow_forward_ios),
                  onPressed: (){},
                ),
              ),
            ),
            Card(
              child: ListTile(
                leading: Icon(Icons.search),
                title: Text('Search'),
                trailing: IconButton(
                  icon: Icon(Icons.arrow_forward_ios),
                  onPressed: (){},
                ),
              ),
            ),
            Card(
              child: ListTile(
                leading: Icon(Icons.search),
                title: Text('Search'),
                trailing: IconButton(
                  icon: Icon(Icons.arrow_forward_ios),
                  onPressed: (){},
                ),
              ),
            ),
          ],
        ),
      ) : drawer,

    );
  }
}
