// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:panorama_p/UI/Widgets/hotelView.dart';
import 'package:panorama_p/UI/Widgets/navBar.dart';

import 'Screen.dart';

class BestOffers extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Screen(
      title: 'Best Offers',
      body: ListView.builder(
          itemCount: 20,
          itemBuilder: (context,i){
            return HotelView();
          }
      ),
      index: 2,
    );
  }
}
